module.exports.stream = function stream (conditionalFn, actionFn) {
	while(conditionalFn() === true){
		actionFn();
	}

};

module.exports.sumNumbers = function sumNumbers (numbers) {
	var length = numbers.length;
	var counter = 0;
	var total = 0;
	while(counter < length){
		total += numbers[counter];
		counter++;
	}
	return total;
};


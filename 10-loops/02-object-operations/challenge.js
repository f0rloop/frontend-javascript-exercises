module.exports.copy = function copy (obj) {
	var newObj = {};
	for(var i in obj) {
		newObj[i] = obj[i];
	}
	return newObj;
};

module.exports.extend = function extend (dest, src) {
	// var destProperties = {};
	for(var prop in src){
		dest[prop] = src[prop];
	}
	console.log(dest);
	return dest;
};

module.exports.hasElems = function hasElems ( object, elemArray) {
	if (elemArray.length == 0) {
		return true;
	} else {
		
		for (var i = 0; i < elemArray.length; i++) {
			var result;
			if (object.hasOwnProperty(elemArray[i])) {
				result = true;
				console.log(elemArray[i] + " true statement " + result);
			} else {
				console.log(elemArray[i] + " false statement " + result);
				return false;
			}
		};
		return result;
	}
};

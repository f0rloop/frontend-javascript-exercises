module.exports.sumNumbers = function sumNumbers (arrayNum) {
	var sum = 0;
	for (var i = 0; i < arrayNum.length; i++) {
		sum += arrayNum[i];
	}
	return sum;
};

module.exports.splitAndLowerCaseString = function splitAndLowerCaseString (string) {
	var splitArray = string.split(",");
	console.log(splitArray);
	var splitLower = [];
	for (var i = 0; i < splitArray.length; i++) {
		var downCase = splitArray[i].toLowerCase();
		splitLower.push(downCase);
	}
	return splitLower;
};

module.exports.addIndex = function addIndex (array) {
	var indexedArray = [];
	for (var i = 0; i < array.length; i++) {
		var index = i + " is " + array[i];
		indexedArray.push(index);
	}
	return indexedArray;
};

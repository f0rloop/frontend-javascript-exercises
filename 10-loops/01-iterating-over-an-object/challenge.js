module.exports.getKeys = function getKeys (obj) {
	var keys = [];
	for(var key in obj) {
		keys.push(key);
	}
	return keys;
};

module.exports.getValues = function getValues (object) {
	var values = [];
	for(var k in object){
		var value = object[k];
		values.push(value);
	}
	return values;
};

module.exports.objectToArray = function objectToArray (o) {
	var os = [];
	for(var i in o){
		var objArray = i + " is " + o[i];
		os.push(objArray);
	}
	return os;
};
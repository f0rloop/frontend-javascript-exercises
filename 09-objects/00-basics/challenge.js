module.exports.createCourse = function createCourse (title, duration, students) {
	var course = {
		title: title,
		duration: duration,
		students: students
	};
	return course;
};

module.exports.addProperty = function addProperty (object, newProp, newValue) {
	if (object.hasOwnProperty(newProp)) {
		return object;
	} else {
		object[newProp] = newValue;
		return object;
		}
};

module.exports.formLetter = function formLetter (obj) {
	var letter = obj;
	console.log(letter.msg);
    // should return "Hello James,\n\nThings are well.\n\nSincerely,\nRichard"
	return "Hello " + letter.recipient + ",\n" + "\n" + letter.msg + "\n\n" + "Sincerely,\n" + letter.sender;
};

module.exports.canIGet = function canIGet (item, money) {
	var items = {
		"MacBook Air": 999,
		"MacBook Pro": 1299,
		"Mac Pro": 2499,
		"Apple Sticker": 1
	};

	return items[item] <= money;
	
};
module.exports.formLetter = function(firstName, senderName, message) {
return "Hello " + firstName + ",\n" + "\n" + message + "\n" + "\nSincerely," + "\n" + senderName;
// "Hello James,\n\nThings are well.\n\nSincerely,\nRichard"
};

module.exports.sliceItAndCombineIt = function(bigString, startA, endA, startB, endB) {
var text = bigString;
var concat1 = text.substring(startA, endA);
var concat2 = text.substring(startB, endB);

return concat1 + concat2;
};

module.exports.findFirstMatch = function(text, searchString) {
var index = text.indexOf(searchString);
return index;
};

module.exports.findLastMatch = function(text, searchString) {
var index = text.lastIndexOf(searchString);
return index;
};

module.exports.substringBetweenMatches = function(text, searchString) {
var searchLength = searchString.length;
var firstSearch = text.indexOf(searchString);
var lastSearch = text.lastIndexOf(searchString);
var index = text.substring(firstSearch + searchLength, lastSearch);
return index;
};
module.exports.reversePlusOne = function reversePlusOne(array) {
	var newArray = array.reverse();
	newArray.unshift(1);
	return newArray;
};

module.exports.plusesEverywhere = function plusesEverywhere (array) {
	return array.join('+');
};

module.exports.arrayQuantityPlusOne = function arrayQuantityPlusOne (args) {
	return args.length + 1;
};

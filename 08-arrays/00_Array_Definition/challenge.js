module.exports.newArray = function newArray (arg1, arg2, arg3, arg4) {
	var array = [arg1, arg2, arg3, arg4];
	return array;
};

module.exports.firstAndLast = function firstAndLast (args) {
	var lastIndex = args.length - 1;
	var output = [args[0], args[lastIndex]];
	return output;
};
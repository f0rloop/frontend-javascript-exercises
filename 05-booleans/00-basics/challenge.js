module.exports.equalStrings = function(stringOne, stringTwo) {
var result = stringOne === stringTwo;
return result;
};

module.exports.notEqual = function(one, two) {
var result = one !== two;
return result;
};

module.exports.inBetween = function(lower, middle, upper) {
return (lower < middle) &&  (upper > middle);
};

module.exports.outsideRanges = function(number) {
var resultOne = (!(number >= 10 && number <= 20) && !(number > 42 && number <= 75)) &&
				(!(number > 1 && number < 6));
return resultOne;
};


   // ```javascript
   //  nameAndPrice('NYTimes', 1) // returns true
   //  nameAndPrice('LATimes', 0) // returns false
   //  nameAndPrice('NYTimes', 0) // returns false
   //  ```

module.exports.nameAndPrice = function(name, price) {
var newspaper = ((name == "LATimes" || name == "NYTimes") && price >= 1);
return newspaper;
};